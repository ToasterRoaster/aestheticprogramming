// setting up two angles
let angle = 60;
let angle2 = 180;

// creating a canvas, setting anglemode to degrees instead of radians, and 60fps
function setup(){
  createCanvas(windowWidth,windowHeight);
  angleMode(DEGREES);
  frameRate(60);
}

//Background, ellipses, rotation using angles above.
function draw(){
  background("#eb34e8");

  translate(400,400);
  rotate(angle);
  ellipse(50,20,20,20);
  rotate(angle2);
  ellipse(50,20,20,20);

  angle = angle + 1
  angle2 = angle2 + 0

}

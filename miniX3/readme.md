## My miniX3
This one was by far the hardest one so far, and I don't even think I did that good a job. I had a lot of difficulties with basically everything, 


Link to the sketch:
https://toasterroaster.gitlab.io/aestheticprogramming/miniX3/

Link to the code:
https://gitlab.com/ToasterRoaster/aestheticprogramming/-/blob/main/miniX3/sketch.js

Screenshot:
<img src=screenshot.png>

I don't really have much to say this time around. The program is pretty simple.
I start with creating two variables for angles, I setup my canvas, define my angleMode as degrees
(as opposed to the standard radians), set a framerate to 60, draw a background with a pink colour, move everything to a central
point, create two ellipses and make them rotate.

I had several issues doing this simple rotation. Firstly, gettig them to rotate in the first place was a really difficult task, and when I first tried with around 5 ellipes, the speeds and angles were completely off. For some godforsaken reason, the ellipses had different speeds, wildly differing and making the whole thing appear insane. Moreover, I could not for the life of me make them create a standard circle. When trying a lot of different angles that I thought would create a nice circular pattern failed, i opted to just brute force it into being functional by deleting ellipses, and eventually just settling on two ellipses. It's hard doing math when sick.

// Hi! This is the code of my second miniX.

function setup() {
  // Simple set up of a canvas. No movement, so no custom framerate.
  createCanvas(windowWidth,windowHeight);
}

function draw() {
  // First, the background
  background(255,149,220);
  // The head of the first emoji. I use hex instead of RGB for colour.
  fill("#145425");
  ellipse(210,210,210,210);
  // The ears? horns? up to interpretation
  triangle(60,80,107,190,150,125);
  triangle(360,80,314,190,270,125);

  // The facial features. White color, two circles and an Arc. Happy orc :)
  fill("#ffffff");
  ellipse(255,185,35,);
  ellipse(165,185,35,);
  arc(210, 240, 75, 60, 0, PI);

  // Same thing as the first head. Green color for orc :)
  fill("#145425");
  ellipse(500,210,210,210);

  // Sad orc. Same as before, white filling for eyes, but no filling
  // for the arc/mouth. It is also the opposite of the smile,
  // Quite literally. It uses negative pi.
  fill("#ffffff");
  ellipse(540,185,35,);
  ellipse(450,185,35,);
  noFill();
  arc(500, 270, 75, 60, -PI, 0);
}

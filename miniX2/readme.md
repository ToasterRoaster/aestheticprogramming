## Hi! Welcome to my second miniX!

### Today, we're talking about emojis, and my homemade emojis!

Let's first go over the links and the formalia.

First off, the link to my program: https://toasterroaster.gitlab.io/aestheticprogramming/miniX2/

Secondly, link to the code: https://gitlab.com/ToasterRoaster/aestheticprogramming/-/blob/main/miniX2/RUNME.js

Thirdly, a screenshot of the program:
<img src=screenshot.png>

### The code itself

Going over the code, I have primarily made use of the basics.
I only use createCanvas() in my setup, where i use windowWidth and windowHeight as the parameters. There is no need to
set up a custom framerate, as there is no movement in this miniX.

My draw function is where the more intersting things are hiding. I make use of my signature pastel pink background, which
I adore, and then I use the fill command to select a color for the head of my first emoji. I think it is important to note
that I don't use the standard Red, Green and Blue values to determine the colors. I instead make use of the hex colors,
typically a # with a bunch of numbers and letters behind it. Examples here are #145425, which is a dark green and #ffffff,
which is pure white. I use the hex value because it is much easier to find a specific color that you want to use.
It makes it very easy to just type in "color picker" into Google and use the tool to find the desired color, then copy the
hex code and insert it as a string into the fill() tool. Like so: fill("#145425");.

I make use of this fill tool multiple time to switch color as I draw different things. With the first fill, I create a green
head and ears. I do this with the ellipse and triangle commands. After I draw those, I switch color to white by using the fill
tool again. I draw the two eyes, just as two smaller ellipses and then I also draw an arc to represent a full smile.

The arc is the bane of my existence. I managed to get it to work, but when you are bad at math, like I am, it is very
confusing to operate with radians and pi. I thank the p5js reference for explaining the arc command, but while I got it to
work, I barely understand how it functions, all due to my incompetence in math.

This concludes the creation of my first emoji, which I henceforth will dub the "Happy orc". Or is a demon? It is up
to interpretation. The ears look more like horns, thanks to being triangles, so whichever mythical being you think it
resembles more, is completely fine with me. I will personally interpret it as an orc.

For the second orc, I did essentially all the same things as I did before, just moving the entire thing to the right.
Notably however, the arc function is used differently here. While a full smile makes perfect sense, I doubt it is physically
possible to frown while showcasing all your teeth. Therefore, the mouth is closed, and the arc is left unfilled.
The process of turning that smile upside down was tedious and filled with confusion. I ended up attempting a negative pi, which
worked, and simply stuck with that instead of trying to make the frown look more realistic.

Why is this orc sad? Well, perhaps it is because it lacks ears/horns, or maybe it is for some other reason? Who knows.

And with the second orc finished, so is the program.

### Why the orcs/mythical beings?
When thinking of emojis, we typically think of drawings and cartoons of humans. Almost all of the moods we wish to express
over text messages are represented by humans, or at least human-esque figures. My idea with these emojis come from my
current fascination with fantasy stories (Thanks, Elden Ring!), but also by some already existing emojis we can find on
our phones and in our apps.

My first inspiration is the current "cat" emojis we can see on our phones. There aren't a lot of moods expressed here,
but you could certainly show some of the more basic emotions through them. Happy, laughing, heart-eyes, shocked, et cetera.

Similarly, there is the monkey trio. Monkey with hands over eyes, with hands on side of head, and with hands covering mouth.

Unless you wish you could change the fur color of the cat, I believe it is a very good example of a neutral emoji. It is
disconnected from the intricacies of human politics and identity. It is quite literally not a human. It is another lifeform.

Another very big inspiration comes from the primarily Russia-based messaging app "VK", short for "VKontakte". It is a
social media platform that I use to communicate with my friends from the country, and one thing I noticed about the culture
of that platform is their use, or lack therof, of the standard emojis. Instead, they rely heavily on the stickers that
VK has created in collaboration with various animation studios like Disney. Many of these stickers are not human, and are
instead simply humanoid in nature. Looking on the front page of the stickers page, I can see a sticker called "Snookie", a
sort of reindeer made of snow. Similarly there is Eustace the bird and a package of stickers called "Mice hugs".

Instead of tackling the problem of human identity, it seems that VK has attempted to simply walk around the issue, by
using non-human figures to represent human emotions. This does still leave the problem of representation and identity
unanswered, but I think that is the point. It is trying to create an alternative, which is also what I have done.

I picked something simple to create, something humanoid that can still express the feelings and emotions that we have,
but is itself disconnected from our daily politics. The orc, I think, is sufficiently both human and non-human, that it can
express a variety of emotions while having its literal skin be green, something that is (for now) very unhuman. I also
think orcs are simply cool beings, with their own aesthetics to consider.

All in all, I think the orc is a way of circumventing the modern day political issues of emojis. Is it an elegant way to do
it? Not particularly. It is very much avoiding the issue, but I also think it has potential in its own right. Lord knows that
VKontakte has commercialised it with several "skins" as we know them from the gaming scene leaking into the sticker store.
And in general, I think it is cool representing one's emotions using mythical beings.

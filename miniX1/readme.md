## Hi!
### Welcome to my readme!

In this file I will briefly explain the code of my RUNME file that I have submitted for this week's MiniX.

Let's first get the boring stuff out of the way and get a few links ready.

First up on the agenda, a screenshot of my sketch:
<img src=image.png>

Secondly, a link to my sketch (I pray this works):
https://toasterroaster.gitlab.io/aestheticprogramming/miniX1/

And finally, a link to my code:
https://gitlab.com/ToasterRoaster/aestheticprogramming/-/blob/main/miniX1/RUNME.js

### So! I guess this is where I explain my sketch a little, no?

I think my program is a little exciting, because I actually do something in my code BEFORE I do the setup function.
What I do, is that I "let" some variables, which is essentially the way you create variables in JavaScript.
The variables I create are "slider" and "fr", short for framerate. I also add a number to the fr variable, which is 144.
That number will be important in the future.

Now, it's at this point where I do my function setup()
I do 3 important things in my setup.
I use the frameRate function, which sets the frameRate (how many times per second my sketch updates) to 144.
Additionally, I create a slider by using createSlider, slider.position, and slider.style.
Finally, I create my canvas, which should be exactly the width and height of the device that's opening the file.


In my function draw(), I finally set up my background. Why did I do that in my draw and not in setup? That'll have to do
with motion, which I will explain later.
Here I also set up another variable, which I have a feeling that I could have done earlier, but I am unsure about that.

I set the colour of my shapes using the fill command and then I use a hex instead of RGB value, just to try something new.

Finally, I set up the shapes themselves. The triangle in itself is not all too exciting, it is just three points
connected by lines and filled up with the color in the fill command (which is a dark blue). What's much more exciting is
the ellipse, which has its height determined by the value of the slider. That means my sketch is interactive!

This does probably mean that my screenshot should have been a video, but luckily you, the reader, can always try it out
yourself!

This concludes my readme. Thanks for reading!

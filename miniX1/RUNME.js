// Hi! Welcome to my code. It's not incredibly exciting, but it's here!
let slider;
let fr = 144;
// Here i create a slider and framerate variable.

function setup() {
  // put setup code here
  frameRate(fr);
  slider = createSlider(0,255,100);
  slider.position(40,40);
  slider.style('width', '200px');
  createCanvas(windowWidth,windowHeight);
  print("Hah, you checked the code, nerd");
  /*
  I tried to manually guess a canvas size that was the same as my device's
   width and height. Then i discovered windowWidth & windowHeight :)
   I also made the background pastel pink - it's my favourite colour!

   Additionally, this is where I set up the slider and framerate.
  */


}

function draw() {
  // put drawing code here
  // Draw updates every frame at 60fps
  background(255,149,220);
  let val = slider.value();
  fill("#2e7eff");
  ellipse(420,420,420,val);
  triangle(100,100,420,69,69,420);
/*
This is where I simply draw everything! It happens faster than by default
thanks to the frameRate method i set up earlier. This means that, if your
device/monitor is capable of displaying 144 Frames per Second, you should
be able to notice the sketch feeling more "smooth" when changing the value
of the slider! A little more boringly, I also set up my background and shape
colors, as well as my two shapes. The first shape, the triangle, is boring
and has nothing special about it. However, the ellipse uses the slider
to change its height.
*/
}
